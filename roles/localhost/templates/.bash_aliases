alias goMicroservices='cd /drives/drive0/vagrant/machines/microservices'
alias goProjects='cd /drives/drive0/vagrant/machines/projects'
alias goAnsible='cd /home/{{ bash_username }}/repos/projects/ansible'