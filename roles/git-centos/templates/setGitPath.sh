#!/bin/sh

# set git path to latest installed git version 2.13.0 (/usr/local/git/bin/git)
# instead of preinstalled git version 1.8.3 (/bin/git)

export PATH=/usr/local/git/bin:$PATH